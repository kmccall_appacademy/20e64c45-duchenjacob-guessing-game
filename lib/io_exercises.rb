# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
require "byebug"

def guessing_game
  guesses = 0
  guess = -1
  number = rand(1..100)
  until guess == number
    print "Guess a number between 1 and 100: "
      guess = gets.chomp.to_i
        puts "#{guess} is too high" if guess > number
        puts "#{guess} is too low" if guess < number

    guesses += 1
  end

puts "#{guess} is correct! You guessed #{guesses} times."
end

#
# def shuffle
#
#
# end

########################################################
# if __FILE__ == $PROGRAM_NAME
#   guessing_game
# end

#
# def guessing_game
#   number = rand(1..100)
#   guess = -1
#   guesses = 0
#   until guess == number
#     print "Guess a number between 1 and 100: "
#     guess = gets.chomp.to_i
#
#     puts "#{guess} is too high" if guess > number
#     puts "#{guess} is too low" if guess < number
#
#     guesses += 1
#   end
#
#   puts "#{guess} is correct! You guessed #{guesses} times."
# end
#
#
def shuffle
  print "Enter filename to shuffle: "
  filename = gets.chomp

  file_line_by_line = File.readlines(filename)
  shuffled = file_line_by_line.shuffle

  new_name = ""
  if filename.include?(".")
    new_name = "#{filename.partition(".")[0]}-shuffled.txt"
  else
    new_name = "#{filename}-shuffled.txt"
  end

  File.open(new_name, "w") { |f| shuffled.each { |l| f.puts l } }
end
